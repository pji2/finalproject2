commit f7db1083d27061737ca82708a024def729127bc3
Merge: 34a4d06 beb279d
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Sun Apr 30 20:20:00 2017 -0400

    Removing all files

commit 34a4d06ef1c407b864745e3dab4be2f61dfdea22
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Sun Apr 30 20:07:06 2017 -0400

    Changes to movement

commit beb279db0690af073a9a62d64246e69c8e8e98fc
Author: Peter Ji <pji2@jhu.edu>
Date:   Sun Apr 30 17:39:26 2017 -0400

    fixed bugs in pawn promotion

commit ab501058eb061554a520ee3f2e628cb18fa5b606
Author: Peter Ji <pji2@jhu.edu>
Date:   Sun Apr 30 02:12:00 2017 -0400

    new folder

commit ee93cd162f2221dd688cded8d4ba66c02a7d1cc8
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Thu Apr 27 11:28:24 2017 -0400

    Finished with checked() and special movement to make Queen. In order to use makeQueen() in class Board, there will be a different return to makeMove() and then call makeQueen() to replace the piece.

commit 9903ae5322e794e5e7ee1a7e08550177b9ca31aa
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Thu Apr 27 10:53:51 2017 -0400

    Finish isChecked()

commit 98a1a7b38ceb9303ae98de00f5c264df98c4531a
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Thu Apr 27 01:52:12 2017 -0400

    Halfway though testing checked()

commit 58cc396018f49ee83e8cd6ce0f2b7a3115c3c72a
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Thu Apr 27 00:16:29 2017 -0400

    Still uncompilable until having the correct isChecked() and special movement for the pawn. May need to add to makeMove() in the main function

commit 61f8288535133c54b4ee81cfe653b8e8c4c2ad17
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Wed Apr 26 17:22:51 2017 -0400

    Fix wrong error codes and isCaptured in the wrong place

commit f6fda2f509b279354e4c405340c5c5bd8ca14294
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Wed Apr 26 14:10:18 2017 -0400

    Fix the pawn

commit ba6fdc3739bc408d67ed27b744bd3f99fb726824
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Wed Apr 26 13:37:12 2017 -0400

    Delete the piece as it got captured

commit 763c58eb9d8b514daab9c3cb6ca347fa940b45f8
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Wed Apr 26 00:21:23 2017 -0400

    Make slight changes to Chess.h, Game.h. Finish all movement, blocked, captured. Waiting to test the code against the real board

commit 0919895ef7222aa0f15d946b8c0fadb5374a3b2c
Author: Peter Ji <pji2@jhu.edu>
Date:   Tue Apr 25 16:50:39 2017 -0400

    save, load, non-move user input except toggle board

commit a226f6788f0cf4dcb231680a2b15dfcd3de91493
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Tue Apr 25 01:23:02 2017 -0400

    Finish all the blocked possibilities. Wait for capture

commit 3efdd193f3531d9b75abbc482cdc2a64f6f0832d
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Tue Apr 25 01:22:47 2017 -0400

    Finish all the blocked possibilities. Wait for capture

commit 34af3071378873908ee1f771336f893408da2568
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 23:37:45 2017 -0400

    Make small changes to Chess.h, add blocked to Rook. still have errors...

commit baed7641c021bef10f8b815c29a6dbd85075d1ec
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 22:34:58 2017 -0400

    Add changes to Chess.h

commit f7f84567e70430cd7055b3e9284e1d947f150384
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 15:11:34 2017 -0400

    finished all the changes

commit 23b239e1945419ab84687c664a4acfed54aa67f6
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 15:10:17 2017 -0400

    readd new Chess.cpp

commit 54fd3422c88d84b42d81b4746dc06e2803153e3d
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 15:07:55 2017 -0400

    all

commit aa7ccc3265c1de671c703e88cdebf9f974dbe356
Merge: f694952 1dd5c90
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 14:57:15 2017 -0400

    Fix merge conflict and adjustment to Movement.cpp

commit f694952891c56f7a7819aba8714667fe64e1422b
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 14:50:30 2017 -0400

    Make a little change in Movement.cpp

commit 1dd5c9027c205513688a8f12a0f9bdd52cce4e67
Merge: b8791c5 8ff119f
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 14:42:35 2017 -0400

    Merge branch 'master' of bitbucket.org:tnguy188/finalprojectcs120
    afsdf

commit b8791c52214a985b93499457fb46cfd2026198cf
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 14:01:37 2017 -0400

    made pawn:validmove function in chess.cpp

commit 8ff119f9b378035cb13d37c129ce14728690f34d
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 13:52:02 2017 -0400

    Add comments on Chess.cpp file from the instructors. Style fix as well but can just ignore. Update Terminal.h

commit 31b67c4f49b60f0f50a91d340afe47aa83bfe5a4
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 13:39:18 2017 -0400

    Update from Piazza regarding these two files

commit f4255b504a61b00d3d6cc661d5e1dc71d575a01e
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 04:01:43 2017 -0400

    :

commit 101c770a3c0bf7270810f11f4626f8abb7f5d3eb
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 03:23:51 2017 -0400

    take a look at Pawn:makemove in Chess.cpp

commit f3648b863789e6fd43909f5b651bbb7b35656380
Author: Peter Ji <pji2@jhu.edu>
Date:   Mon Apr 24 01:19:51 2017 -0400

    change chess.run, chess.makemove

commit 5249032060ecfc1a6766c98d7de4b75dbfb385b7
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Mon Apr 24 00:49:58 2017 -0400

    add a function occupied in Piece (within Game.h) to check if the next step will land on an occupied place by the same player or not. Also finish all the basic movement, but not checked and not movethrough

commit 7cc12cf0e6f71a5d281d5662dda78e02748ff800
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Sun Apr 23 16:52:16 2017 -0400

    Working on the movement of the pieces in the Movement.cpp

commit f8af8325c20c1a886fee163e33383edf825a46ed
Author: Peter Ji <pji2@jhu.edu>
Date:   Sun Apr 23 15:38:19 2017 -0400

    chess.run,chess.makeMove

commit d438b93c6b063bf40b35ef83d31ce5ae21c3b821
Author: Peter Ji <pji2@jhu.edu>
Date:   Sun Apr 23 05:06:07 2017 -0400

    changes to chess.run, chess.makeMove

commit ae5036377765bc1e0c66f25b61d9eddbad14ebbb
Author: Peter Ji <pji2@jhu.edu>
Date:   Fri Apr 21 16:24:25 2017 -0400

    First commit from Peter

commit f849ce183ac01a74dba2301e6599b2d953de05a5
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Fri Apr 21 15:25:37 2017 -0400

    Add all skeleton files

commit 293f729b51026f948b000b8ec4ac4b5243f709c1
Author: Tan_Nguyen <tnguy188@jhu.edu>
Date:   Fri Apr 21 14:02:13 2017 -0400

    Firsh push to test
