CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: main.o Chess.o Board.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o main.o -o chess

unittest: unittest.o Chess.o Board.o
	$(CXX) $(CXXFLAGS) unittest.o Chess.o Board.o -o unittest

main.o:	main.cpp Game.h Chess.h Prompts.h 
unittest.o: unittest.cpp Game.h Chess.h Prompts.h
Board.o: Game.h
Chess.o: Game.h Chess.h Prompts.h

clean:
	rm *.o chess unittest

