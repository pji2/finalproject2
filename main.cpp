#include <iostream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include <fstream>

int main() {
    Prompts::menu();
    std::string option;
    std::getline(std::cin,option);
    ChessGame chess;
    while (option != "1" && option != "2") {
        std::cout << "Invalid menu option" << std::endl;
        Prompts::menu();
        std::getline(std::cin,option);
    }
    if (option == "1") {
        chess.setupBoard();
        //chess.displayBoard();
        chess.setKingCoords(Position(4, 0), WHITE);
        chess.setKingCoords(Position(4, 7), BLACK);
    } else {
        std::string fileName;
        Prompts::loadGame();
        std::cin >> fileName;
        std::ifstream ifs;
        ifs.open(fileName);
        if (!ifs.is_open()) {
            Prompts::loadFailure();
            return 0;
        }
        std::string line;
        ifs >> line;
        if (line != "chess"&&line!="CHESS") {
            Prompts::loadFailure();
            return 0;
        }
        int a;
        std::string b;
        int c;
        ifs >> a;
        chess.setTurn(a + 1);
        while (ifs >> a >> b >> c) {
            Position p = Position(tolower(b[0]) - 97, b[1] - 49);
            Player pL = (a) ? BLACK : WHITE;
            chess.initPiece(c, pL, p);
        }
        ifs.close();
        //find king and store position in private member of King:Piece class
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece* pc = chess.getPiece(Position(i, j));
                if (pc != nullptr && pc->id() == KING_ENUM && pc->owner() == WHITE) {
                    chess.setKingCoords(Position(i, j), WHITE);
                }
                if (pc != nullptr && pc->id() == KING_ENUM && pc->owner() == BLACK) {
                    chess.setKingCoords(Position(i, j), BLACK);
                }
            }
        }
        //chess.displayBoard();
    }
    int s = 1;
    bool display=false;
    while (s) {
        s = chess.run();
        if(s==2){display=!display;}
        if(display==true){
        chess.displayBoard();}
    }  
}
