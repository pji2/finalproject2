TEAM:
Tan Nguyen tnguy188
Peter Ji pji2

DESIGN:
We used dynamic dispatch to find valid moves for each piece. The valid move functions, <aPiece>::validMove, returns an error code in enum to the given makeMove function in Chess.cpp. This makes the majority of the game logic. After every move, we run isChecked to check for checks. This is based on the king's position, and we run across the diagonals, horizontals, and verticals (special cases for knight and pawns) until we hit the first piece and see if it is a threat. Queen was reduced to a rook+bishop. We run checkmate test if isCheck is true, and we run stalemate test during each loop to make sure the player has no available moves for himself and his king is not in check. Test file stalemate5.txt is a good example of this. Our checkmate and stalemate algorithms are based on "fake moving" every one of the player's piece correctly and seeing if he is out of check as a result. The test moves are reversed before return.  

COMPLETENESS:
Very complete, with castling and pawn promotion

SPECIAL NOTES/INSTRUCTIONS:
1) For the method Board::isChecked(args), even though it seems like we have code duplication, we have tried to reduce the code duplication as much as possible. There are no other ways (that we have thought of) that could reduce more while functioning the same way. 

2)Our unit testing only takes care of the movement for the pieces and interactive between them. The rest of the tests is checked in end-to-end testing. 

3) We have created a mystery piece to test the board. The mystery piece can jump two squares left, right, up down, but cannot jump over pieces like the knight. You can see this by loading in arg6a.txt. Please be careful when you add your own mystery piece.

4) Even though some data is passed by value, they are small.

5) Tam's computer prints out the board without end-line stripes, so we have attached a png picture for that. Some computers have end-line stripes
end-to-end test files:
in#.txt - user commands
out#.txt - output.
arg1.txt - checkmate
arg2.txt - castling
arg3.txt - checkmate
arg4.txt - checkmate
arg5.txt - checkmate
arg6.txt - mysterypiece
arg7.txt - stalemate
arg8.txt - stalemate
arg9.txt - stalemate
arg10.txt - cool stalemate
arg11.txt - stalemate
arg12.txt - get out of check by capture + white queenside castling
