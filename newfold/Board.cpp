#include <cassert>
#include <cctype>
#include <iostream>
#include <algorithm>
#include <vector>
#include "Game.h"
#include "Prompts.h"
#include "Chess.h"

using vit = std::vector<Position>::iterator;

//use to check if it is a checkmate or stalemate situation
enum ESCAPECHECK {
    NO_ESCAPE = 0,
    ESCAPE_SUCCESS = 1
};

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i = 0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i = 0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.(not call prompts if out of bounds yet).
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

//create a vector of positions that a knight may be able to move
std::vector<Position> Board::aroundKnight(Position start)const {
    std::vector<Position> temp;
    temp.push_back({ start.x + 2, start.y - 1 });
    temp.push_back({ start.x + 2, start.y + 1 });
    temp.push_back({ start.x + 1, start.y + 2 });
    temp.push_back({ start.x + 1, start.y - 2 });
    temp.push_back({ start.x - 2, start.y - 1 });
    temp.push_back({ start.x - 2, start.y + 1 });
    temp.push_back({ start.x - 1, start.y + 2 });
    temp.push_back({ start.x - 1, start.y - 2 });
    return temp;
}

//create a vector of positions that a king may be able to move
std::vector<Position> Board::aroundKing(Position start)const {
    std::vector<Position> temp;
    temp.push_back({start.x + 1, start.y + 1});
    temp.push_back({start.x + 1, start.y});
    temp.push_back({start.x + 1, start.y - 1});
    temp.push_back({start.x, start.y - 1});
    temp.push_back({start.x - 1, start.y - 1});
    temp.push_back({start.x - 1, start.y});
    temp.push_back({start.x - 1, start.y + 1});
    temp.push_back({start.x, start.y + 1});
    return temp;
}

//test if the king is checked from specific position by a specific piece
int Board::checkedType(Position temp, Player name, std::vector<int> type)const {
    if (this->validPosition(temp)) {
        if (this->getPiece(temp) != nullptr) {
            Piece* tempPiece = this->getPiece(temp);
            if (tempPiece->owner() == name) return BREAKSEARCH;
            else {
                using viter = std::vector<int>::iterator;
                for (viter it = type.begin(); it != type.end(); ++it) {
                    if (tempPiece->id() == *it) return THREAT;
                }
                return BREAKSEARCH;
            }
        } else return CONTINUE;
    } else return CONTINUE;
}

bool Board::isCheckedRook (Position start, Player name) const {
    for (unsigned int i = start.x + 1; i < 8; i++) {
        Position temp = { i, start.y };
        int j = checkedType(temp, name, { ROOK_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = start.x - 1; i + 1 != 0; i--) {
        Position temp = { i, start.y };
        int j = checkedType(temp, name, { ROOK_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = start.y + 1; i < 8; i++) {
        Position temp = { start.x, i };
        int j = checkedType(temp, name, { ROOK_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = start.y - 1; i + 1 != 0; i--) {
        Position temp = { start.x, i };
        int j = checkedType(temp, name, { ROOK_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    return false;
}

bool Board::isCheckedBishop(Position start, Player name) const {
    for (unsigned int i = 1; start.x + i < 8 && start.y + i < 8; i++) {
        Position temp = { start.x + i, start.y + i };
        int j = checkedType(temp, name, { BISHOP_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = 1; start.y + i < 8 && start.x - i + 1 != 0; i++) {
        Position temp = { start.x - i, start.y + i };
        int j = checkedType(temp, name, { BISHOP_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = 1; start.x + i < 8 && start.y - i + 1 != 0; i++) {
        Position temp = { start.x + i, start.y - i };
        int j = checkedType(temp, name, { BISHOP_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    for (unsigned int i = 1; start.x - i + 1 != 0 && start.y - i + 1 != 0; i++) {
        Position temp = { start.x - i, start.y - i };
        int j = checkedType(temp, name, { BISHOP_ENUM, QUEEN_ENUM });
        if (j == BREAKSEARCH) break;
        else if (j == THREAT) return true;
    }
    return false;
}

bool Board::isCheckedPawn(Position start, Player name) const {
    if (name == WHITE) {
        Position temp = { start.x - 1, start.y + 1 };
        int j = checkedType(temp, name, { PAWN_ENUM });
        if (j == THREAT) return true;
        else { //no threat
            Position temp = { start.x + 1, start.y + 1 };
            int j = checkedType(temp, name, { PAWN_ENUM });
            if (j == THREAT) return true;
        }
    } else { //black king
        Position temp = { start.x - 1, start.y - 1 };
        int j = checkedType(temp, name, { PAWN_ENUM });
        if (j == THREAT) return true;
        else { //no threat
            Position temp = { start.x + 1, start.y - 1 };
            int j = checkedType(temp, name, { PAWN_ENUM });
            if (j == THREAT) return true;
        }
    }
    return false;
}

bool Board::isCheckedKnight(Position start, Player name) const {
     std::vector<Position> temp = aroundKnight(start);
        for (vit it = temp.begin(); it != temp.end(); ++it) {
            int j = checkedType(*it, name, { KNIGHT_ENUM });
            if (j == THREAT) return true;
    }
        return false;
}

bool Board::isCheckedKing(Position start, Player name) const{
    std::vector<Position> temp = aroundKing(start);
        for (vit it = temp.begin(); it != temp.end(); ++it) {
            int j = checkedType(*it, name, { KING_ENUM });
            if (j == THREAT) return true;
        }
    return  false;
}

//check from diffrent direction if the king is checked or not. 
bool Board::isChecked(Position start, Player name) const {
    if (isCheckedRook(start,name)) return true; 
    if (isCheckedBishop(start,name)) return true;
    if (isCheckedPawn(start,name)) return true;
    if (isCheckedKnight(start,name)) return true;
    if (isCheckedKing(start,name)) return true;
    return false;
}

void Board::makeQueen(Position start, Position end, Player name) {
    //delete temp; causes error in board destructor
    Piece* temp = this->getPiece(start);
    delete temp;
    m_pieces[index(start)] = nullptr;
    this->initPiece(QUEEN_ENUM, name, end);
}


int Board::commonAnalyze(Player loser, Position pos, Position temp, 
        Piece* testp, Piece* capPiece,int goodMove, bool runStalemate, Position kingPos) {
    if ((!runStalemate && this->validPosition(temp) && capPiece == nullptr) || 
            (capPiece != nullptr && capPiece->owner() != loser)) { //second part runs whether stalemate checking or not
        m_pieces[index(temp)] = testp;
        m_pieces[index(pos)] = nullptr;
        bool check = isChecked(kingPos, loser);
        if (!check && goodMove > 0) { 
            m_pieces[index(pos)] = testp;
            m_pieces[index(temp)] = capPiece; 
            return ESCAPE_SUCCESS;
        }
        m_pieces[index(pos)] = testp;
        m_pieces[index(temp)] = capPiece;
    } else if (this->validPosition(temp) && capPiece == nullptr && runStalemate) {
        m_pieces[index(temp)] = testp;
        m_pieces[index(pos)] = nullptr;
        bool chck = isChecked(kingPos, loser);
        m_pieces[index(pos)] = testp;
        m_pieces[index(temp)] = capPiece;
        if (!chck && goodMove > 0) return 1;
    }
    return 0;
}

bool Board::analyzeKing (Piece* testp, Position pos, Player loser, bool runStalemate) {
    std::vector<Position> temp = aroundKing(pos);
    for (vit it = temp.begin(); it != temp.end(); ++it) {
        Piece* capPiece = this->getPiece(*it);
        if ((!runStalemate && this->validPosition(*it) && capPiece == nullptr) || (capPiece != nullptr
                && capPiece->owner() != loser)) {
            m_pieces[index(*it)] = testp;
            m_pieces[index(pos)] = nullptr;
            bool check = isChecked(*it, loser); //move the king
            if (!check) { //restore, not a checkmate
                m_pieces[index(pos)] = testp;
                m_pieces[index(*it)] = capPiece;
                return true;
            }
            m_pieces[index(pos)] = testp; //if check
            m_pieces[index(*it)] = capPiece;
        } else if (this->validPosition(*it) && capPiece == nullptr && runStalemate) {
            m_pieces[index(*it)] = testp;
            m_pieces[index(pos)] = nullptr;
            bool chck = isChecked(*it, loser);
            m_pieces[index(pos)] = testp;
            m_pieces[index(*it)] = capPiece;
            if (!chck) return true;
        }
    }
    return false;
}

bool Board::analyzeRook(Position kingPos, Piece* testp, Position pos, Player loser,
                        bool runStalemate) {
    for (int check = 0; check < 2; check++) {
        for (unsigned int i = 0; i < 8; i++) {
            Position temp = pos;
            if (check == 0) temp.x = i;
            else if (check == 1) temp.y = i;
            Piece* capPiece = this->getPiece(temp);
            int gdMove = testp->validMove(pos, temp, *this);
            int val = commonAnalyze(loser, pos, temp, testp, capPiece, gdMove, runStalemate, kingPos);
            if (val == 1) return 1;
        }
    }
    return false;
}

bool Board::analyzeBishop(Position kingPos, Piece* testp, Position pos, Player loser,bool runStalemate) {
    for (int check = 0; check < 2; check++) {
        for (int i = 0; i < 8; i++) {
            Position temp = pos;
            if (check == 0) {
                int diff = temp.y - temp.x;
                temp.x = i;
                temp.y = i + diff; //run across diagonals
            } else if (check == 1) {
                int sum = temp.y + temp.x;
                temp.x = i;
                temp.y = sum - i; //run across diagonals
            }
            Piece* capPiece = this->getPiece(temp);
            int gdMove = testp->validMove(pos, temp, *this); //nothing blocking path
            int val = commonAnalyze(loser, pos, temp, testp, capPiece, gdMove, runStalemate, kingPos);
            if (val == 1) return 1;
        }
    }
    return false;
}

bool Board::analyzeKnight(Position kingPos, Piece* testp, Position pos, Player loser,
                          bool runStalemate) {
    std::vector<Position> temp = aroundKnight(pos);
    for (vit it = temp.begin(); it != temp.end(); ++it) {
        Piece* capPiece = this->getPiece(*it);
        if ((!runStalemate && this->validPosition(*it) && capPiece == nullptr) || (capPiece != nullptr
                && capPiece->owner() != loser)) { //valid position test included in getPiece()
            m_pieces[index(*it)] = testp;
            m_pieces[index(pos)] = nullptr;
            bool check = isChecked(kingPos, loser);
            if (!check) { //restore, not a checkmate
                m_pieces[index(pos)] = testp;
                m_pieces[index(*it)] = capPiece;
                return ESCAPE_SUCCESS;
            }
            m_pieces[index(pos)] = testp;
            m_pieces[index(*it)] = capPiece;
        } else if (this->validPosition(*it) && capPiece == nullptr && runStalemate) {
            int goodMove = testp->validMove(pos, *it, *this);
            m_pieces[index(*it)] = testp;
            m_pieces[index(pos)] = nullptr;
            bool chck = isChecked(kingPos, loser);
            m_pieces[index(pos)] = testp;
            m_pieces[index(*it)] = capPiece;
            if (!chck && goodMove > 0) return 1;
        }
    }
    return false;
}


bool Board::analyzePawn(Position kingPos, Piece* testp, Position pos, Player loser,
                        bool runStalemate) {
    std::vector<Position> temp;
    if (loser == WHITE) {
        temp.push_back({ pos.x, pos.y + 1 });
        if (pos.y == 1) {
            temp.push_back({ pos.x , pos.y + 2});
        }
        temp.push_back({ pos.x + 1, pos.y + 1 }); //validMove checks validity
        temp.push_back({ pos.x - 1, pos.y + 1 });
    } else {
        temp.push_back({ pos.x, pos.y - 1 });
        if (pos.y == 6) {
            temp.push_back({ pos.x , pos.y - 2 });
        }
        temp.push_back({ pos.x + 1, pos.y - 1 }); //validMove checks validity
        temp.push_back({ pos.x - 1, pos.y - 1 });
    }
    for (vit it = temp.begin(); it != temp.end(); ++it) {
        int gdMove = testp->validMove(pos, *it, *this);
        Piece* capPiece = this->getPiece(*it);
        int val = commonAnalyze(loser, pos, *it, testp, capPiece, gdMove, runStalemate, kingPos);
        if (val == 1) return true;
    }
    return false;
}

int Board::analyze(Position kingPos, Piece* testp, Position pos, Player loser, bool runStalemate) {
    if (testp->id() == KING_ENUM) {
        if (this->analyzeKing(testp, pos, loser, runStalemate)) return ESCAPE_SUCCESS;
    }
    if (testp->id() == ROOK_ENUM || testp->id() == QUEEN_ENUM) {
        if (this->analyzeRook(kingPos, testp, pos, loser, runStalemate)) return ESCAPE_SUCCESS;
    }
    if (testp->id() == BISHOP_ENUM || testp->id() == QUEEN_ENUM) {
        if (this->analyzeBishop(kingPos, testp, pos, loser, runStalemate)) return ESCAPE_SUCCESS;
    }

    if (testp->id() == KNIGHT_ENUM) {
        if (this->analyzeKnight(kingPos, testp, pos, loser, runStalemate)) return ESCAPE_SUCCESS;
    }
    if (testp->id() == PAWN_ENUM) {
        if (this->analyzePawn(kingPos, testp, pos, loser, runStalemate)) return ESCAPE_SUCCESS;
    }
    if (runStalemate) return 0;
    return NO_ESCAPE;
}

bool Board::checkMate(Position kingPos, Player loser) {
    std::vector<int> anyEscape;
    std::vector<int>::iterator anEscape;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Piece* toTest = m_pieces[index(Position(i, j))];
            if (toTest != nullptr && toTest->owner() == loser) {
                int Check = analyze(kingPos, toTest, Position(i, j), loser,
                                    false); //found a friendly piece, now test all moves
                if (Check == ESCAPE_SUCCESS) {
                    anyEscape.push_back(1);
                } else {
                    anyEscape.push_back(0);
                }
            }
        }
    }
    anEscape = std::find(anyEscape.begin(), anyEscape.end(), 1);
    if (anEscape != anyEscape.end()) {
        return false;
    } else {
        return true;
    }
}
bool Board::Stalemate(Player p, Position kingPos) {
    std::vector<int> anyPossibleMove;
    std::vector<int>::iterator aPossibleMove;
    Piece* aPiece;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            aPiece = m_pieces[index(Position(i, j))];
            if (aPiece != nullptr && aPiece->owner() == p) { //get player's pieces
                int aPossibleMove = analyze(kingPos, aPiece, Position(i, j), p,true); //finds if any validMove for this piece
                if (aPossibleMove == 1) anyPossibleMove.push_back(1);
                else anyPossibleMove.push_back(0);
            }
        }
    }
    aPossibleMove = std::find(anyPossibleMove.begin(), anyPossibleMove.end(), 1);

    //check to find if there is a move to escape
    if (aPossibleMove != anyPossibleMove.end()) return false;   
    else return true;
}
